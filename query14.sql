create table brand(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    PRIMARY KEY (id)
    );

create table product(
    id INT NOT NULL AUTO_INCREMENT,
    artNumber VARCHAR(50),
    name VARCHAR(50),
    description VARCHAR(150),
    brandId INT,
    PRIMARY KEY (id)
    );

ALTER
    TABLE product
    ADD CONSTRAINT fk_brand_product
    FOREIGN KEY (brandId)
    REFERENCES brand(id)
    ;

INSERT
    INTO brand(name)
    VALUES
    ("Nike"),
    ("Adidas"),
    ("BATA")
    ;

INSERT
    INTO product(artNumber, name, description, brandId)
    VALUES
    ("PD001-S", "Jordan Delta 2", "sepatu nike mantep", 1),
    ("PD002-S", "Air Jordan 4", "sepatu nike Air jordan", 1),
    ("PD003-S", "Superstar", "sepatu adidas superstar", 2),
    ("PD004-F", "Predator", "sepatu futsal adidas predator", 2)
    ;