package id.co.nexsoft.formative14_brand_dan_product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.formative14_brand_dan_product.entity.Brand;
import id.co.nexsoft.formative14_brand_dan_product.repository.BrandRepository;

@RestController
public class BrandController {
    @Autowired
    private BrandRepository repo;

    @GetMapping("/brand")
    public List<Brand> getAllBrand(){
        return repo.findAll();
    }

    @GetMapping("brand/{id}")
    public Brand getBrandById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
}
