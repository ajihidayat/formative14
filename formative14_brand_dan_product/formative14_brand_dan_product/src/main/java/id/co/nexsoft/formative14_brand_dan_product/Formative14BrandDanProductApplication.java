package id.co.nexsoft.formative14_brand_dan_product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative14BrandDanProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(Formative14BrandDanProductApplication.class, args);
	}

}
