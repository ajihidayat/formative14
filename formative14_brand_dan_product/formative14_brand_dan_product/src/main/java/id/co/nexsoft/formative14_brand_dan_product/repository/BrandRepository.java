package id.co.nexsoft.formative14_brand_dan_product.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.formative14_brand_dan_product.entity.Brand;

public interface BrandRepository extends CrudRepository<Brand, Integer>{
    Brand findById(int id);
    List<Brand> findAll();
}
