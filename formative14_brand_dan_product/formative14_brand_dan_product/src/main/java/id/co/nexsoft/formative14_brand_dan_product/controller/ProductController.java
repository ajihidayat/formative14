package id.co.nexsoft.formative14_brand_dan_product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.formative14_brand_dan_product.entity.Product;
import id.co.nexsoft.formative14_brand_dan_product.repository.ProductRepository;

@RestController
public class ProductController {
    @Autowired
    private ProductRepository repo;

    @GetMapping("/product")
    public List<Product> getAllProduct(){
        return repo.findAll();
    }

    @GetMapping("/product/{id}")
    public Product getProductById(@PathVariable(value = "id") int id){
        return repo.findById(id);
    }
}
