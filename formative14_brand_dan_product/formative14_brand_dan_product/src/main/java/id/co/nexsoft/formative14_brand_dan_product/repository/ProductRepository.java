package id.co.nexsoft.formative14_brand_dan_product.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


import id.co.nexsoft.formative14_brand_dan_product.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Integer>{
    Product findById(int id);
    List<Product> findAll();    
}
